all: i3 code misc

misc:
	sudo apt install -y bat pick
	cp bashrc ~/.bashrc
	cp gitconfig ~/.gitconfig

i3:
	mkdir -p ~/.config/i3
	mkdir -p ~/.config/i3blocks
	cp i3-dotfiles/i3config ~/.config/i3/config
	cp i3-dotfiles/i3blocks.conf ~/.config/i3blocks/config
	sudo apt install -y xfce4-appfinder network-manager arandr i3blocks git
	mkdir -p ~/programs && git clone https://github.com/vivien/i3blocks-contrib.git ~/programs/i3blocks-contrib
	i3-msg reload

code:
	mkdir -p  ~/.config/Code/User/
	cp vs-code/settings.json ~/.config/Code/User/settings.json
	cp vs-code/keybindings.json ~/.config/Code/User/keybindings.json
	bash vs-code/install_code_extensions.bash vs-code/code-extensions.txt
