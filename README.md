# Dotfiles

Important dotfiles are stored here.
Make sure to clone this repository into `~/repos`.


# Install

1. Install [vscode](https://code.visualstudio.com/download).
2. `cd <repo>`
3. `make all`
4. Restart.